# mash

This is a rebuild of the mash package from EPEL.

We rebuild to work around the issue seen in [https://cern.service-now.com/service-portal?id=ticket&n=INC3210027](INC3210027) when using cephfs as a staging directory
